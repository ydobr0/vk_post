#!/usr/bin/env python3
import requests
import re
import subprocess
import os
from random  import randint
import logging

block_links = '/home/dob/Desktop/vk_post/block_db'
logging.basicConfig(format='%(asctime)s %(message)s', filename='/home/dob/Desktop/vk_post/log_pipec.log',level=logging.DEBUG)
def downloadContent():
    logging.info('start downloadContent function ')
    get_page = requests.get('https://pipec.info/')
    #sources_lst = re.findall('(https\:\/\/pipec.*[0-9]*-?_?smehu.*html)\"\>.*?С?c?', get_page.text)
    sources_lst = re.findall('(https\:\/\/pipec.*[0-9]*-?_?smehu.*html?|https.*tut_konechno.*html|https.*nashe|https.*durdom.*html|https.*noname.*html|https.*snova_v_shkolu.html)\"\>.*?C?c?', get_page.text)
    print(sources_lst)
    for source in range(0, len(sources_lst)):
        with open(block_links, 'r') as db:
            deny_links = db.read().split()
        if sources_lst[source] in deny_links:
            logging.info(sources_lst[source] +  ' in block_list.')
            continue
        else:
            logging.info( sources_lst[source] +' no in  block_list. Do work')
            with open(block_links, 'a') as db:
                db.write(sources_lst[source] + '\n')
            logging.info('parse content from PPC ....')
            get_cont = requests.get(sources_lst[source])
            imgs = re.findall('https\:\/\/i?\.?pipec.info\/[0-9]*\/.*jpg', get_cont.text)

            for image in range(0, len(imgs)):
                logging.info('get content from ppc ....')
                r = requests.get(imgs[image], stream=True)
                name = re.findall('px\/([0-9].*jpg)', imgs[image])
                if r.status_code == 200:
                    logging.info('OK! Start download ' + " ".join(name))
                    with open('/home/dob/Desktop/vk_post/image/' + " ".join(name), 'wb') as f:
                        for chunk in r:
                            f.write(chunk)
def rand_position():
    position =['center','west','east', 'southeast', 'northwest']
    gen_pos = randint(0, len(position)-1)
    return position[gen_pos]

    


def watermark():
    print("water")
    logging.info('start watermark function ....') 
    db = '/home/dob/Desktop/vk_post/image/'
    if len(os.listdir(db)) == 0:
        logging.info(db + ' - is empty.Start download')
        downloadContent()
    
        count = 0
        for file in os.listdir(db):
            logging.info('add watermark to' + file)
            print("mark")
            pos = rand_position()
            file=file.replace('(','\(')
            file=file.replace(')','\)')
            command = r'/usr/bin/composite -dissolve 20% -geometry +8+0 -gravity ' + pos +' /home/dob/Desktop/vk_post/water/water.png ' + db +  file + ' /home/dob/Desktop/vk_post/post_file/'+ file + '_' + str(count) + '.jpg'
            print(file + '_' + str(count) + '.jpg')
            print(command)
            subprocess.Popen(command, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            count+=1
    else:
        logging.info(db + '- is not empty. Remove images')
        for item in os.listdir(db):
            if item.endswith(".jpg"):
                os.remove(os.path.join(db, item))
#downloadContent()
watermark()
#vk_api1.main1()
