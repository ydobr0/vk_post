#!/usr/bin/env python3
import vk_api
from vk_api import VkUpload
import os
import time
import logging
import random
logging.basicConfig(format='%(asctime)s %(message)s', filename='/home/dob/Desktop/vk_post/log_vk-api_post.log',level=logging.DEBUG)
token='<TOKEN_ID>'
group_id = '-<GROUP_ID>'
me= '<ME_ACC_ID>'
vk_session = vk_api.VkApi(token=token)
vk_session._auth_token()
upload = VkUpload(vk_session)
db_plain='/home/dob/Desktop/vk_post/post_file'
db_rek='/home/dob/Desktop/vk_post/post_rek'
rek_file='/home/dob/Desktop/vk_post/rek_tmp'
def acc(photos,db,img) :
    try:
        logging.info('Try get access_key')
        access_k = upload.photo_wall(db + '/' + photos[img])[0]['access_key']
    except:
        logging.info('Try again get access_key sleep 5sec')
        time.sleep(5)
        access_k = upload.photo_wall(db + '/' + photos[img])[0]['access_key']
    return access_k

def doc_acc(docs,db,doc):
    print(str(docs)+" "+str(db)+" "+ str(doc))
    access_k = upload.document_wall(db + '/' +docs[doc])
    return access_k

def plain(db):
    photos = os.listdir(db)
    img = random.randint(0, len(photos)-1)
    if photos[img].endswith('gif'):
         doc_list = upload.document_wall(db + '/' + photos[img])['doc']['id']
#         print(doc_list)
         doc='doc' + me + '_' + str(doc_list)
         vk_session.method("wall.post", {
            'owner_id':  group_id, 
            #'message': 'Test!',
            #'access_key': access_k,
            'from_group': '1',
            'attachments': doc
         })
    else:
        access_k = acc(photos,db,img)
        photo_list = upload.photo_wall(db + '/' + photos[img])[0]['id']
        print(photo_list)
        att = 'photo' + me + '_' + str(photo_list)
        logging.info('send to vk '+ att )
        vk_session.method("wall.post", {
            'owner_id':  group_id, 
            #'message': 'Test!',
            'access_key': access_k,
            'from_group': '1',
            'attachments': att
        })
    logging.info('Delete img' + db + "/" + photos[img])
    os.remove(db + "/" + photos[img])

def rek(db):
    logging.info ('ADS in progress')
    photos = os.listdir(db)
    img = random.randint(0, len(photos)-1)
    access_k = acc(photos,db,img)
    photo_list = upload.photo_wall(db + '/' + photos[img])[0]['id']
    att = 'photo' + me + '_' + str(photo_list)
    logging.info('send ADS to vk '+ att )
    vk_session.method("document", {
        'owner_id':  group_id, 
        'mark_as_ads': 1,
        #'message': 'Test!',
        'access_key': access_k,
        'from_group': '1',
        'attachments': att
    })
    logging.info('Delete ADS img ' + db + "/" + photos[img])
    os.remove(db + "/" + photos[img])


if __name__ == "__main__":
    if len(os.listdir(db_rek) ) != 0 and not os.path.exists(rek_file):
        rek(db_rek)
        with open(rek_file, 'w') as file:
            file.write('1')
    elif len(os.listdir(db_rek) ) != 0 and os.path.exists(rek_file):
        plain(db_plain)
        os.remove(rek_file)
    else:
        plain(db_plain)

        
